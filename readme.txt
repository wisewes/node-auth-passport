Added this:
20394 Sector 3


Added file in 09/05/2014
- fixed this
- fixed that
- fixed those
- fixed them


4.16.2014

- fix for problem with users who are chatting, clicking on their name may or may not fetch their object data
- improved way of fetching user object data (markup for modal window now in bottom of <body> tag)
- JS module created for displaying popup messages/alerts
