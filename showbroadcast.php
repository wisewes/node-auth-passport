<?php 

require_once('lib/ParseQueryManager.php');
session_start();
$queryManager = $_SESSION["parseManager"];


if(isset($_GET['specificbroadcast'])) {
    
    $specificbroadcast = $_GET['specificbroadcast'];
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://api.parse.com/1/classes/_User?where=%7B%22objectId%22%3A%22' . $specificbroadcast . '%22%7D&include=current_broadcast');
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Parse-Application-Id: KiFaNKo92CrkhgLiqD6BwgoQCjjj7mbV7redGUVv', 'X-Parse-Master-Key: 8ARUanFVQuMjzqy4lCLT1Fo8JgJmeHFpUSuhAohK'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, '10');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    $content = trim(curl_exec($ch));
    curl_close($ch);

    $json_decoded = json_decode($content);
    $jsonarray = $json_decoded->results;
    

    foreach ($jsonarray as $jsonarrays) {
        $userId = $jsonarrays->objectId;
        $name = $jsonarrays->name;
        $email = $jsonarrays->email;
        $username = $jsonarrays->username;
        $viewer_count = $jsonarrays->current_broadcast->viewer_count;
        $broadcast_id = $jsonarrays->current_broadcast->objectId;
        $avatar1 = $jsonarrays->avatar->url;
        $avatar2 = isset($jsonarrays->avatar_url) ? $jsonarrays->avatar_url : null;
        $avatar3 = "images/defaultavatar.png";
        $viewercount = $jsonarrays->current_broadcast->viewer_count;
        $chat_service = $jsonarrays->current_broadcast->chat_info->service;
        $flagged = $jsonarrays->current_broadcast->flagged;
        $verified = $jsonarrays->verified;
        $private = $jsonarrays->private;
        $session_token = $jsonarrays->sessionToken;
        $broadcast_orientation = $jsonarrays->current_broadcast->video_orientation;
        $jabber_id = strstr($jsonarrays->current_broadcast->chat_info->room_jid, "@", true);
        $coins = $jsonarrays->total_coins;

        //structure <div class based on variables to outline it correctly
        if ($flagged) {
            ?>
            <div class="phone-flagged" id="holder<?php echo $broadcast_id; ?>">
        <?php } else if ($verified == 1) { ?>
             <div class="phone-featured" id="holder<?php echo $broadcast_id; ?>">
        <?php } else if ($private == 1) { ?>
             <div class="phone-private" id="holder<?php echo $broadcast_id; ?>">
        <?php } else { ?>
             <div class="phone" id="holder<?php echo $broadcast_id; ?>">
        <?php }
        ?>

            <!-- for jabber chat -->
            <input type="hidden" id="<?php echo $broadcast_id; ?>chat-type" value="<?php echo $chat_service; ?>"/>
            <input type="hidden" id="<?php echo $broadcast_id; ?>jabber" value="<?php echo $jabber_id; ?>"/>

            <div class="phone-header"> 
                <div class="phone-header-avatar" style=""> 
                    <img src="thumb.php?src=<?php
                        if (isset($avatar1)) {
                            echo $avatar1;
                        } elseif (isset($avatar2)) {
                            echo $avatar2;
                        } else {
                            echo $avatar3;
                        }
                        ?>&h=50&w=50">
                </div>

                <div class="phone-header-username-count"> 
                    <strong><?php echo $username; ?></strong>
                    <br /> 
                    <span><small><?php echo "Hanging w/ " . $viewercount . " people"; ?></small></span>  
                </div>

                <div class="phone-header-close-button" id="<?php echo $broadcast_id; ?>"> 
                    <button class="btn btn-xs btn-danger" id="doesntmatter<?php echo $broadcast_id; ?>" class="okay"> X </button> 
                </div>
            </div>
                

                <?php
                //pick stream based on video orientation

                switch ($broadcast_orientation) {

                    case 2:
                        $videourl = "http://medl.alldigital.net/rtmplive/" . $broadcast_id . "/playlist.m3u8";
                        $videoClassType = "videoType2";
                        break;

                    case 1:
                        $videourl = "http://medl.alldigital.net/rtplive/" . $broadcast_id . "/playlist.m3u8";
                        $videoClassType = "videoType1";
                        break;

                    case 0:
                        $videourl = "http://medl.alldigital.net/rtplive/" . $broadcast_id . "/playlist.m3u8";
                        $videoClassType = "videoType0";
                }
                ?>
                

                <div class="video-wrapper">
                    <input type="hidden" id="orientationTypeHidden" value="<?php echo $broadcast_orientation; ?>"/>
                    
                    <video id="<?php echo $broadcast_id ?>" class="<?php echo $videoClassType; ?>" height="200" width="200" muted autoplay>
                        <source src="<?php echo $videourl ?>" type="application/vnd.apple.mpegurl"/>
                    </video>
                    
                   <div class='video-refresh-wrapper' style='display:none;'>
                       <button type="button" class="btn btn-info btn-xs">
                           <span class="glyphicon glyphicon-refresh"></span>
                       </button>
                   </div>
                </div>


                <!-- start buttons -->

                <div class="buttons-grouped">
                    <div class="mute-button" id="<?php echo $broadcast_id; ?>">
                        <button class="btn btn-warning btn-xs" style="width:100px;" >Mute</button >
                    </div>

                    <div class="unmute-button" id="<?php echo $broadcast_id; ?>">
                        <button class="btn btn-warning btn-xs" style="width:100px;">Unmute</button >
                    </div>

                    <div class="update-chat-button" id="<?php echo $broadcast_id; ?>">
                        <button class="btn btn-primary btn-xs updatechat" id="<?php echo $broadcast_id; ?>" style="width:200px;">Update Chat</button>
                    </div>

                    <div class="more-button" id="<?php $broadcast_id; ?>">
                        <a data-toggle="modal" href="#modal<?php echo $userId; ?>">
                            <button class="btn btn-success btn-xs">More</button > 
                        </a> 
                    </div>
                </div>


                <!-- grab chat -->
                <?php
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://api.parse.com/1/classes/Comment?where=%7B%22%24relatedTo%22%3A%7B%22object%22%3A%7B%22__type%22%3A%22Pointer%22%2C%22className%22%3A%22Broadcast%22%2C%22objectId%22%3A%22' . $broadcast_id . '%22%7D%2C%22key%22%3A%22comments%22%7D%7D&include=user');
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Parse-Application-Id: KiFaNKo92CrkhgLiqD6BwgoQCjjj7mbV7redGUVv', 'X-Parse-Master-Key: 8ARUanFVQuMjzqy4lCLT1Fo8JgJmeHFpUSuhAohK'));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_TIMEOUT, '3');
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
                $content2 = trim(curl_exec($ch));
                curl_close($ch);

                $json_decoded2 = json_decode($content2);
                $jsonarray2 = $json_decoded2->results;

                ?>
                
                <div class="chatbox" id="chat<?php echo $broadcast_id; ?>">
                    
                </div> <!-- end chatbox -->
                

                
                <!-- MODAL WINDOW FOR BROADCASTER'S MORE BUTTON -->
                
                <div class="modal fade" id="modal<?php echo $userId; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close userModalPopup" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h3 class="modal-title">
                                    <div id='<?php echo $userId; ?>' class='modifyUsernameWrapper'><p><?php echo $username; ?></p></div>                                
                                    <button type="button" class="btn btn-info btn-xs modifyUsernameButton" id="<?php echo $userId; ?>-usernameButton" disabled="disabled" style="display:none;">Update</button>
                                </h3>
                            </div>

                            <div class="modal-body" style="font-size: 14px;">

                                <input type="hidden" id="disciplineUserId" value="<?php echo $userId; ?>">
                                <input type="hidden" id="disciplineEmail" value="<?php echo $email; ?>">
                                <input type="hidden" id="disciplineBroadcastId" value="<?php echo $broadcast_id; ?>">
                                <input type="hidden" id="disciplineUsername" value="<?php echo $usermame; ?>">
                                <input type="hidden" id="disciplineSessionToken" value="<?php echo $session_token; ?>">

                                <div class="row">
                                    <img style="float:left; margin-right:20px" src="thumb.php?src=<?php
                                        if (isset($avatar1)) {
                                            echo $avatar1;
                                        } elseif (isset($avatar2)) {
                                            echo $avatar2;
                                        } else {
                                            echo "images/defaultavatar.png";
                                        }
                                        ?>"> 

                                    <div class="modal-body-content col-md-9">
                                        <input type='hidden' id='<?php echo $userId; ?>-Email' value='<?php echo $emailchat; ?>'/>
                                        <dl class="dl-horizontal">
                                            <dt>Name</dt><dd><?php echo $name; ?></dd>
                                            <dt>Username</dt><dd><?php echo $username; ?></dd>
                                            <dt>Email</dt><dd><?php echo $email; ?></dd>
                                            <dt>User ID</dt><dd><?php echo $userId; ?></dd>
                                            <dt>Broadcast ID</dt><dd><?php echo $broadcast_id; ?></dd>
                                            <dt>Total Coins</dt><dd><?php echo $coins; ?> <button type="" class="btn btn-link btn-xs addMoreCoinsTriggerLink">Add More Coins</button></dd>
                                        </dl>
                                    </div> <!-- end modal-body-content -->
                                </div>
                                
                                <div class="row col-md-12 addCoinsWrapper" style="display: none;">
                                    <h5>User Coins</h5>
                                    <div class="col-md-2">
                                         <label for="coinAddAmount">Add #</label>
                                        <input type="text" class="form-control" id="coinAddAmount"/>
                                    </div>
                                    <div class="col-md-8">
                                        <label for="coinReason">Reason</label>
                                        <input type="text" class="form-control" id="coinReason"/>
                                        <span class="help-block">Reason is optional.</span>
                                    </div>
                                    <div class="col-md-2">
                                        <button type="" style="position:relative;top:25px;" class="btn btn-success addCoinsButton"><strong>+ Coins</strong></button>
                                    </div>
                                </div>

                                <div class="row">
                                    <div id="function-button-wrapper">
                                        <button type="button" class="btn btn-danger testSuspendUser" id="<?php echo $userId; ?>">Warn / Suspend / Private</button>
                                        <button type="button" class="btn btn-primary avatarremove" id="<?php echo $userId; ?>">Remove Avatar</button>
                                        <button type="button" class="btn btn-success makefeatured" id="<?php echo $userId; ?>">Make Featured</button>
                                        <button type="button" class="btn btn-warning emailUser" id="<?php echo $email; ?>">Email User</button>
                                    </div>
                                </div>

                                <!-- empty div that gets populated with email form or suspend actions -->    
                                <div id="<?php echo $userId; ?>-suspend-wrapper" class="suspend-user-wrapper" style="display:none;"></div>          

                            </div> <!-- end modal body -->
                        </div> <!-- end content -->
                    </div> <!-- end dialog -->
                </div> <!-- end modal -->
                    
                 <script>

                     //auto-load comments for broadcast - trigger on update-button click
                    //set for 2 seconds after div has been loaded into DOM
                    var timerSingle;
                    
                    var broadcastID = $('#disciplineBroadcastId').val();
                    //alert(broadcastID);
                    
                    $(document).ready(function() {
                        if(timerSingle) {
                            clearTimeout(timer);
                        }
                        timerSingle = window.setTimeout(function() {
                             
                            $('#' + broadcastID + '.update-chat-button').trigger('click', function() {
                            });
                        }, 2000);
                    });

                </script>
                
            </div> <!-- phone holder -->

        <?php
    } //end foreach
}
?>