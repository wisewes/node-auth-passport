<?php
/**
 * ParseQueryManager.php
 * Created by Chris
 * Date: 7/17/13
 * Time: 12:57 PM
 */

require_once("constants.php");
require_once("PermissionManager.php");
require_once("ConnectionManager.php");
require_once("User.php");

define("PARSE_OBJECT_CHANNEL_FEATURED","M5aGVr9Ctu");

class ParseQueryManager
{
    /***
     * @var ConnectionManager connection manager to handle communication to parse server
     */
    private $_connection;

    /**
     * @var string
     */
    private $_sessionToken;

    /**
     * @var User
     */
    private $_user;

    private  $_debugEnabled;

    private $_permissionManager;

    public function __construct($environment = PARSE_ENVIRONMENT_PRODUCTION)
    {
        $this->_connection = new ConnectionManager(CONNECTION_TYPE_PARSE, $environment);
        $this->_permissionManager = new PermissionManager();
        $this->_debugEnabled = ini_get("display_errors");
    }

    /**
     * Create Parse REST API call endpoint
     * @param string $path Data Model Name (e.x. _User, ModerationActivity...)
     * @param string $objectID Parse ObjectID
     * @return string URL for Parse REST API end point
     */
    private function createParseURL($path, $objectID)
    {
        if($objectID != "")
            return "https://api.parse.com/1/$path/$objectID" ;
        return "https://api.parse.com/1/$path" ;
    }

    public function setSessionToken($token)
    {
        $this->_sessionToken = $token;
    }

    /**
     * Search for Parse Objects
     * @param $objectTypeName
     * @param $params
     * @param array $includes optional array of relation name, required if related-object should be included on the return
     * @param string $order optional field name to order the result by
     * @return string REST API results as a json string
     */
    private function searchForObject($objectTypeName, $params, $includes = null, $order = null)
    {
        $url = $this->createParseURL("classes/$objectTypeName", "");
        $where = array("where" => json_encode($params));
        if($includes != null)
        {
            $include = implode(",", $includes);
            $where["include"] = $include;
        }
        if($order != null)
        {
            $where["order"] = $order;
        }

        return $this->_connection->sendQuery($url, $where, $this->_sessionToken, "GET");
    }

    /**
     * Search for Parse Objects
     * @param string $objectTypeName Data Model Class Name (e.x. _User, ModerationActivity...)
     * @param array $params associative array for search parameters
     * @param int $limitStart start of result (e.x. for 5th result, pass in 5)
     * @param int $resultsCount limiting the result count
     * @param array $includes optional array of relation name, required if related-object should be included on the return
     * @param string $order optional field name to order the result by
     * @param string $includeOnlyFields optional field name to include (but excludes everything else)
     * @return string REST API results as a json string
     */
    private function searchForObjectWithLimit($objectTypeName, $params, $limitStart, $resultsCount, $includes = null, $order = null, $includeOnlyFields = null)
    {
        $url = $this->createParseURL("classes/$objectTypeName", "");
        $where = array();
        if($params != null && count($params) > 0)
        {
            $where["where"] = json_encode($params);
        }
        if($includes != null)
        {
            $include = implode(",", $includes);
            $where["include"] = $include;
        }
        if($order != null)
        {
            $where["order"] = $order;
        }

        if($includeOnlyFields != null)
        {
            $where["keys"] = $includeOnlyFields;
        }

        $where["limit"] = $resultsCount;
        $where["skip"] = $limitStart;
        return $this->_connection->sendQuery($url, $where, $this->_sessionToken, "GET");
    }

    /**
     * @param string $objectName Type name of the object
     * @param string $objectID ParseObjectID
     * @param array $params associative array of parameters to update
     * @return bool
     */
    private function updateObject($objectName, $objectID, $params)
    {
        $url = $this->createParseURL("classes/$objectName", $objectID);
        $response =  $this->_connection->sendQuery($url, $params, $this->_sessionToken);
        $result = json_decode($response);
        if(property_exists($result, "code"))
        {
            //retry once if it times out - for some reason the update call from Parse likes to timeout for no reason
            if($result->code == 142)
            {
                $response =  $this->_connection->sendQuery($url, $params, $this->_sessionToken);
                $result = json_decode($response);
            }
        }
        if(!property_exists($result, "updatedAt"))
        {
            if($this->_debugEnabled)
            {
                var_dump($response);
            }
            return false;
        }
        return true;
    }


    //*****************Login****************

    /**
     * Login to Parse
     * @param string $username
     * @param string $password
     * @return User
     */
    public function login($username, $password)
    {
        $url = $this->createParseURL("login", "");
        $response = $this->_connection->sendQuery($url, array("username" => urlencode($username), "password" => urlencode($password)), null, "GET");
        $responseDict = json_decode($response, true);

        $permissions = $this->getUserPermission($responseDict["objectId"]);
        $permissionsArray = json_decode($permissions)->results;
        if(count($permissionsArray) > 0)
        {
            $responseDict["roleName"] = $permissionsArray[0]->name;
        }
        $user = new User($responseDict);
        if($user->sessionToken)
        {
            $this->_sessionToken = $user->sessionToken;
            $this->_user = $user;
            $ret = $this->_connection->logAction($user->username, "login " . $_SERVER['REQUEST_URI'] );
        }
        return $user;
    }

    /**
     * Get user permission
     * @param string $userObjectID User model's ObjectID
     * @return string REST API result json string
     */
    public function getUserPermission($userObjectID)
    {
        $url = $this->createParseURL("roles", "");
        $params = array("where" => json_encode(array("users" => array("__type" => "Pointer",
                "className" => "_User",
                "objectId" => $userObjectID))
        )
        );

        $response = $this->_connection->sendQuery($url, $params, $this->_sessionToken, "GET");
        return $response;
    }

    /**
     * Retrieve Role Object
     * @param string $roleName
     * @return string REST API result json string
     */
    public function getRole($roleName)
    {
        $url = $this->createParseURL("classes/_Role", "");
        $param = array("where" => json_encode(array("name" => $roleName)));
        $response = $this->_connection->sendQuery($url, $param, $this->_sessionToken, "GET");
        $roles = json_decode($response);
        if(is_array($roles->results) && count($roles->results) > 0)
        {
            return $roles->results[0];
        }
        return $response;
    }

    /**
     * @param string $userObjectId - User's object ID
     * @return mixed|string user object if found
     */
    public function getUserByUserID($userObjectId, $relatedObjectsToInclude = null)
    {
        if(!$userObjectId || strlen($userObjectId) != 10)
            return null;

        $param = array("objectId" => $userObjectId);
        if($relatedObjectsToInclude != null && is_string($relatedObjectsToInclude))
        {
            $relatedObjectsToInclude = array($relatedObjectsToInclude);
        }
        $response = $this->searchForObject("_User", $param, $relatedObjectsToInclude);
        $user = json_decode($response);
        if($user != null)
        {
            return $user->results[0];
        }
        return $response;
    }
    
    
    
    /**
     * @param string $username - User's username
     * @return mixed|string user object if found
     */
    public function getUserByUsername($username, $relatedObjectsToInclude = null)
    {
        $param = array("username" => $username);
        if($relatedObjectsToInclude != null && is_string($relatedObjectsToInclude))
        {
            $relatedObjectsToInclude = array($relatedObjectsToInclude);
        }
        $response = $this->searchForObject("_User", $param, $relatedObjectsToInclude);
        $user = json_decode($response);
        if($user != null)
        {
            return $user->results[0];
        }
        return $response;
    }
    
    

    /**
     * Call cloud functions
     * @param string $functionName cloud function name
     * @param array $params associative array of cloud function parameters
     * @return bool function executed successful or not
     */
    private function  callCloudCode($functionName, $params)
    {
        $url = $this->createParseURL("functions", $functionName);
        $response = $this->_connection->sendQuery($url, $params, $this->_sessionToken, "POST");
        $json = json_decode($response);
        var_dump($json);
        $succeed = property_exists($json, "result");
        return $succeed;
    }

    /**
     * Send user a warning
     * @param string $username Username
     * @param string $userObjectID User Model's ObjectID (from Parse)
     * @param string $reason the reason of warning - this message will be sent as the email content to the user
     * @return bool
     */
    public function warnUser($username, $userObjectID, $reason)
    {
        $params = array("userId" => $userObjectID,
            "reason" => $reason);
        $result = $this->callCloudCode("warnUser", $params);
        if($result)
        {
            $this->_connection->logAction($this->_user->username, "warn user $username for $reason");
        }
        return $result;
    }

    /**
     * Suspend a user
     * @param string $username Username
     * @param string $userObjectID User Model's ObjectID (from Parse)
     * @param string $reason the reason of suspending - this message will be sent as the email content to the user
     * @param int $duration duration in hours
     * @return bool
     */
    public function suspendUser($username, $userObjectID, $reason, $duration)
    {
        $params = array("userId" => $userObjectID,
            "reason" => $reason,
            "duration" => $duration);
        $result = $this->callCloudCode("suspendUser", $params);
        if($result)
        {
            $this->_connection->logAction($this->_user->username, "suspend user $username for $reason for $duration hours");
        }
        
        return $result;
    }

    /**
     * Ban a user
     * @param string $username Username
     * @param string $userObjectID User Model's ObjectID (from Parse)
     * @param string $reason the reason of banning - this message will be sent as the email content to the user
     * @return bool
     */
    public function banUser($username, $userObjectID, $reason)
    {
        $params = array("userId" => $userObjectID,
            "reason" => $reason);
        $result = $this->callCloudCode("banUser", $params);
        if($result)
        {
            $this->_connection->logAction($this->_user->username, "ban user $username for $reason");
        }
        return $result;
    }

    /**
     * @param string $userID Parse ObjectID for the User Model
     * @return string blank if operation success
     */
    public function unblockUser($userID)
    {
        if(!$this->_permissionManager->userHasPermission($this->_user, PERMISSION_DEVICE_UNBLOCKING))
        {
            return null;
        }
        $params = array("user" => array("__type" => "Pointer", "className" => "_User", "objectId" => $userID));
        $response = $this->searchForObject("ModerationActivity", $params, null, "-createdAt");
        $results = json_decode($response);
        $result = $results->results;
        if(!isset($result))
        {
            return false;
        }
        $params = array("active" => false);
        $result = $this->updateObject("ModerationActivity", $result[0]->objectId, $params);
        $this->_connection->logAction($this->_user->username, "Unblock $userID");
        return $result;
    }

    public function getModerationHistory($userID)
    {
        $params = array("user" => array("__type" => "Pointer", "className" => "_User", "objectId" => $userID));
        $response = $this->searchForObject("ModerationActivity", $params, array("moderator"), "-createdAt");
        return $response;
    }

    /**
     * Remove a relation between 2 objects
     * @param string $sourceObject The name of the object that contains the relationship
     * @param string $sourceObjectID Source Object Parse ObjectID
     * @param string $removeObjectName The name of the object to be removed from the relationship
     * @param string $removeObjectID Removal object Parse ObjectID
     * @return string
     */
    public function removeRelation($sourceObject, $sourceObjectID, $removeObjectName, $removeObjectID)
    {
        $params = array("users" =>
            array("__op"    => "RemoveRelation",
                "objects" => array(array("__type" => "Pointer",
                    "className" => $removeObjectName,
                    "objectId" => $removeObjectID)
                )
            ));

        $url = $this->createParseURL("classes/$sourceObject", $sourceObjectID );
        return $this->_connection->sendQuery($url, $params, $this->_sessionToken, "PUT");
    }

    /**
     * Add a relationship between 2 object
     * @param string $destinationObject The name of the object that owns the relationship
     * @param string $destinationObjectID The object ID of the relationship owner
     * @param string $addingObjectName The name of the object to be added as child
     * @param string $addingObjectID The Parse ObjectID of the object to be added
     * @return string
     */
    public function addRelation($destinationObject, $destinationObjectID, $addingObjectName, $addingObjectID)
    {
        $params = array("users" =>
            array("__op"    => "AddRelation",
                "objects" => array(array("__type" => "Pointer",
                    "className" => $addingObjectName,
                    "objectId" => $addingObjectID)
                )
            ));

        $url = $this->createParseURL("classes/$destinationObject", $destinationObjectID );
        return $this->_connection->sendQuery($url, $params, $this->_sessionToken, "PUT");
    }

    /**
     * Addd user to a channel
     * @param string $userID the Parse ObjectID for the user
     * @param string $channelID the Parse ObjectID of the Channel Data Model
     * @return string
     */
    public function addUserToChannel($userID, $channelID)
    {
        return $this->addRelation("Channel", $channelID, "_User", $userID);
    }

    /**
     * @param string $userID the Parse ObjectID for the user
     * @return string
     */
    public function  addUserToFeaturedChannel($userID)
    {
        return $this->addUserToChannel($userID, PARSE_OBJECT_CHANNEL_FEATURED);
    }

    /**
     * @param string $userID the Parse ObjectID for the user
     * @return string
     */
    public function  removeUserFromFeaturedChannel($userID)
    {
        return $this->removeRelation("Channel", PARSE_OBJECT_CHANNEL_FEATURED, ("_"), $userID);
    }

    /**
     * @param string $updateType Type of update - Add/Remove
     * @param string $roleName the name of the Role (e.x. Administrator, Moderator)
     * @param string $userID Parse ObjectID of the User
     * @return string
     */
    private function updateRole($updateType, $roleName, $userID)
    {
        if(!$this->_permissionManager->userHasPermission($this->_user, PERMISSION_CHANGE_USER_PERMISSION))
        {
            return "";
        }
        $role = $this->getRole($roleName);
        $params = array("users" =>
            array("__op"    => $updateType . "Relation",
                "objects" => array(array("__type" => "Pointer",
                    "className" => "_User",
                    "objectId" => $userID)
                )
            ));

        $url = $this->createParseURL("roles", $role->objectId);
        return $this->_connection->sendQuery($url, $params, $this->_sessionToken, "PUT");
    }

    /**
     * @param string $userID Parse ObjectID of the User
     * @return string
     */
    public function  makeUserAdmin($userID)
    {
        return $this->updateRole("Add", "Administrator", $userID);
    }

    /**
     * @param string $userID Parse ObjectID of the User
     * @return string
     */
    public function makeUserModerator($userID)
    {
        return $this->updateRole("Add", "Moderator", $userID);
    }

    /**
     * @param string $userID Parse ObjectID of the User
     * @return string
     */
    public function removeUserAsAdministrator($userID)
    {
        return $this->updateRole("Remove", "Administrator", $userID);
    }

    /**
     * @param string $userID Parse ObjectID of the User
     * @return string
     */
    public function removeUserAsModerator($userID)
    {
        return $this->updateRole("Remove", "Moderator", $userID);
    }

    /**
     * Get a list of all the users
     * @param int $limitStart start of records
     * @param int $limitResultsCount result count limit
     * @param string $order field name to order the result by
     * @return string
     */
    public function getUserList($limitStart, $limitResultsCount, $order)
    {
        return $this->getUserListMatchingCondition(null, $limitStart, $limitResultsCount, $order);
    }

    /**
     * Get a list of users matching the condition specified
     * @param array $conditions associative array of the conditions a user need to match
     * @param int $limitStart start of records
     * @param int $limitResultsCount result count limit
     * @param string $order field name to order the result by
     * @param string $includeOnlyFields exclude everything else but fields in this variable
     * @return string
     */
    public function getUserListMatchingCondition($conditions, $limitStart, $limitResultsCount, $order, $includeOnlyFields = null)
    {
        $params = array();
        if(isset($conditions))
        {
            foreach($conditions as $key => $value)
            {
                $params[$key] = $value;
            }
        }
        $result =  $this->searchForObjectWithLimit("_User", $params, $limitStart, $limitResultsCount, null, $order, $includeOnlyFields);
        return $result;
    }

    /**
     * @param string $name Username or email address of the user
     * @return string
     */
    public function searchUserByName($name)
    {
        $condition = array("\$or" => array(
            array("s_username" => array("\$regex" => "^\\Q" .  strtolower($name) . "\\E")),
            array("email" =>  "$name"),
        )
        );
        $result = $this->searchForObject("_User", $condition);
        return $result;
    }

    /**
     * @param string $broadcastId objectID of the broadcast object
     * @param array|null $relationsToInclude relationship to include on the return
     * @return mixed broadcast object if found
     */
    public function getBroadcastByBroadcastId($broadcastId, $relationsToInclude = null)
    {
        if(!$broadcastId || strlen($broadcastId) != 10)
        {
            return null;
        }

        $param = array("objectId" => $broadcastId);
        if($relationsToInclude != null && is_string($relationsToInclude))
        {
            $relationsToInclude = array($relationsToInclude);
        }

        $response = $this->searchForObject("Broadcast", $param, $relationsToInclude);
        $broadcasts = json_decode($response);
        if(is_object($broadcasts))
        {
            if(count($broadcasts->results) > 0)
            {
                return $broadcasts->results[0];
            }else
            {
                return null;
            }

        }

        return $response;
    }

    /**
     * @return string JSON REST API response (list of all the broadcasting users)
     */
    public function getCurrentBroadcastingUsers()
    {
        $param = array("broadcasting" => true);
        $include = array("current_broadcast");
        $order = "username";
        $result = $this->searchForObject("_User", $param, $include, $order);
        return $result;
    }

    /**
     * Check to see of a particular broadcast is flagged by user
     * @param array $broadcastArray array of broadcast data object
     */
    public function checkFlaggedBroadcasts(&$broadcastArray)
    {
        $broadCastIDs = array();
        $arrayMap = array();
        foreach($broadcastArray as $broadcast)
        {
            $broadcast->flagged = false;
            array_push($broadCastIDs, array("__type" => "Pointer", "className" => "Broadcast", "objectId" => $broadcast->objectId));
            $arrayMap[$broadcast->objectId] = $broadcast;
        }
        $where = array("toBroadcast" => array("\$in" => $broadCastIDs), "type" => "flag");
        $results = $this->searchForObject("BroadcastActivity", $where);
        if($results)
        {
            $resultsArray = json_decode($results);
            if(isset($resultsArray->results) && count($resultsArray->results) > 0)
            {
                foreach($resultsArray->results as $result)
                {
                    $arrayMap[$result->toBroadcast->objectId]->flagged = true;
                }
            }
        }
    }

    /**
     * @param string $broadcastID Parse ObjectID of the broadcast data model
     * @return string
     */
    public function getBroadcastComments($broadcastID)
    {

        $params = array("\$relatedTo" => array("object" => array("__type" => "Pointer",
            "className" => "Broadcast",
            "objectId" => $broadcastID),
            "key" => "comments"));
        $result = $this->searchForObject("Comment", $params, array("user"));
        return $result;
    }

    /**
     * Get all broadcasts from user
     * @param string $userId Parse ObjectID of the User
     * @param int $limitStart start of records
     * @param int $limitCount result count limit
     * @param string $order optional field name to order the result by
     * @return string
     */
    public function getBroadcastFromUser($userId, $limitStart, $limitCount, $order = null)
    {
        $param = array("user" => array("__type" => "Pointer", "className" => "_User", "objectId" => $userId));
        $result = $this->searchForObjectWithLimit("Broadcast", $param, $limitStart, $limitCount, null, $order);
        return $result;
    }


    /**
     * @param int $limit Number of records to return (e.x. Top 10)
     * @param DateTime $dateStart Date to start counting the broadcast
     * @param DateTime $dateEnd  Date to end counting the broadcast
     * @param bool $uniqueUser if false, it will return top 10 broadcast regardless who was the broadcaster (may contains duplicate users)
     * @return string JSON Response
     */
    public function getBroadcastWithTopLikesCount($limit, $dateStart, $dateEnd, $uniqueUser = true)
    {
        $param = array("start_time" => array("\$gt" => array("__type" => "Date", "iso" => $dateStart->format("Y-m-d\TH:i:s.u\Z")), "\$lt" => array("__type" => "Date", "iso" => $dateEnd->format("Y-m-d\TH:i:s.u\Z"))));
        $result = $this->searchForObjectWithLimit("Broadcast", $param, 0, $limit, array("user"), "-like_count");
        $results = json_decode($result)->results;

        if(!$results)
        {
            return array();
        }

        $users = array();
        foreach($results as $broadcast)
        {
            $key = $broadcast->user->objectId;
            if(!array_key_exists($key, $users))
            {
                $users[$key] = $broadcast;
            }
        }
        while(count($users) < $limit && $uniqueUser)
        {
            $userIds = array();
            foreach($users as $userId => $user)
            {
                array_push($userIds, array("__type" => "Pointer", "className" => "_User", "objectId" => $userId));
            }
            $param["user"] = array("\$nin" => $userIds);
            $result = $this->searchForObjectWithLimit("Broadcast", $param, 0, $limit, array("user"), "-like_count");
            $results = json_decode($result)->results;
            foreach($results as $broadcast)
            {
                $key = $broadcast->user->objectId;
                if(!array_key_exists($key, $users))
                {
                    $users[$key] = $broadcast;
                }
            }
        }

        return array_slice(array_values($users), 0, $limit);
    }


    /**
     * @param int $limit Number of records to return (e.x. Top 10)
     * @param DateTime $dateStart Date to start counting the broadcast
     * @param DateTime $dateEnd  Date to end counting the broadcast
     * @param bool $uniqueUser if false, it will return top 10 broadcast regardless who was the broadcaster (may contains duplicate users)
     * @return string JSON Response
     */
    public function getBroadcastWithTopViewersCount($limit, $dateStart, $dateEnd, $uniqueUser = true)
    {
        $param = array("start_time" => array("\$gt" => array("__type" => "Date", "iso" => $dateStart->format("Y-m-d\TH:i:s.u\Z")), "\$lt" => array("__type" => "Date", "iso" => $dateEnd->format("Y-m-d\TH:i:s.u\Z"))));
        $result = $this->searchForObjectWithLimit("Broadcast", $param, 0, $limit, array("user"), "-viewer_count");
        $results = json_decode($result)->results;
        if(!$results)
        {
            return array();
        }
        $users = array();
        foreach($results as $broadcast)
        {
            $key = $broadcast->user->objectId;
            if(!array_key_exists($key, $users))
            {
                $users[$key] = $broadcast;
            }
        }
        while(count($users) < $limit && $uniqueUser)
        {
            $userIds = array();
            foreach($users as $userId => $user)
            {
                array_push($userIds, array("__type" => "Pointer", "className" => "_User", "objectId" => $userId));
            }
            $param["user"] = array("\$nin" => $userIds);
            $result = $this->searchForObjectWithLimit("Broadcast", $param, 0, $limit, array("user"), "-viewer_count");
            $results = json_decode($result)->results;
            foreach($results as $broadcast)
            {
                $key = $broadcast->user->objectId;
                if(!array_key_exists($key, $users))
                {
                    $users[$key] = $broadcast;
                }
            }
        }

        return array_slice(array_values($users), 0, $limit);
    }

    /***************************************************
     * Coins Operations
     ***************************************************/

    /**
     * @param string $broadcastID
     * @param string $userId
     * @param string $username
     * @param int $durationSelected
     * @param int $amountSpent
     * @param int $currentBalance
     * @return bool
     */
    public function logCoinsBroadcastTransaction($broadcastID, $userId, $username, $durationSelected, $amountSpent, $currentBalance)
    {
        return $this->_connection->logCoinsTransaction($userId, $username, "Broadcast", null, $amountSpent, $currentBalance, $durationSelected, $broadcastID);
    }

    /**
     * @param string $userId
     * @param int $amount
     * @param string $reason - the reason why the coins is awarded
     * @return bool
     */
    public function addCoinsForUser($userId, $amount, $reason = null)
    {
        //only allow admin to add coins for now, integrating ACL later
        if(!$this->_user->isAdmin())
        {
            die("Not Authorized");
        }

        //1st retrieve the user
        $user = $this->getUserByUserID($userId);
        if(!$user)
        {
            if($this->_debugEnabled)
            {
                echo "user doesn't exist";
            }
            return false;
        }


        //2nd update the coins

        //use master key
        $this->_connection->useMasterKey = true;

        $success = null;
        $neverHadCoins = !property_exists($user, "total_coins");
        if($neverHadCoins)
        {
            $success = $this->updateObject("_User", $userId, array("total_coins" => 250));
        }else
        {
            $success = $this->updateObject("_User", $userId, array("total_coins" => array("__op" => "Increment", "amount" => $amount)));
        }

        if(!$success)
        {
            if($this->_debugEnabled)
            {
                echo "unable to update user object " . $success;
            }
            return false;
        }

        //3rd record the transaction
        return $this->_connection->logCoinsTransaction($userId, $user->username, "FreeCoins",
            null, $amount, $user->total_coins + $amount, null, null, $this->_user->username, $reason);
    }

    public function updateBroadcastTransaction($broadcastId, $actualDuration)
    {
        return $this->_connection->updateBroadcastDurationForBroadcast($broadcastId, $actualDuration);
    }

    /**
     * Make an user account verified
     * @param string $userId
     * @param bool $verified
     * @return  bool success or not
     */
    public function makeUserVerified($userId, $verified)
    {
        if(!$this->_permissionManager->userHasPermission($this->_user, PERMISSION_DEVICE_UNBLOCKING))
        {
            return false;
        }

        $this->_connection->useMasterKey = true;
        $result =  $this->updateObject("_User", $userId, array("verified" => filter_var($verified, FILTER_VALIDATE_BOOLEAN)));
        if($result === true)
        {
            $this->_connection->logAction($this->_user->username, "Verified user " . $userId . ($verified ? " true" : " false"));
        }

        return $result;
    }

    /**
     * Batch update users
     * @param array $batchedRequests requests to update objects, see https://parse.com/docs/rest#objects-batch for format
     * @return string
     */
    public function batchUpdate($batchedRequests)
    {
        $url = $this->createParseURL("batch", null);
        $this->_connection->useMasterKey = true;
        $result = $this->_connection->sendQuery($url, $batchedRequests, $this->_sessionToken, "POST", true);
        return $result;
    }


    /**
     * Mass Follow Users
     * @param array $followUserIds userIds of the users to follow
     * @param string $userId userId of the follower
     * @return query results
     */
    public function  massFollowUser($followUserIds, $userId)
    {
        $params = array("userIds" => $followUserIds, "redirectedFromAWS" => true, "followerUserId" => $userId);
        $this->_connection->useMasterKey = true;
        $result = $this->callCloudCode("massFollowUserViaREST", $params);
        return $result;
    }

    public function getUserActivity($userId, $activityType)
    {
        $params = array("fromUser" => array("__type" => "Pointer", "className" => "_User", "objectId" => $userId),
            "type" => $activityType);
        $this->_connection->useMasterKey = true;
        $objects = $this->searchForObjectWithLimit("UserActivity", $params, 0, 2000);
//        $objects = $this->searchForObject("UserActivity", $params);
        return $objects;
    }

    /**
     * Ban user from chat - Note: this does not prevent user from getting out of the chat room and comes back in.
     * Suspend/Block user and call this function if preventing user from coming back to chat is desired
     * @param $username
     * @param $userObjectId
     * @param $reason
     * @return bool
     */
    public function banUserChat($username, $userObjectId, $reason)
    {
        $params = array("userId" => $userObjectId, "reason" => $reason);
        //ar_dump($params);
        $this->_connection->useMasterKey = true;
        $result = $this->callCloudCode("banJabberUser", $params);
        
        if($result)
        {
            $this->_connection->logAction($this->_user->username, "ban user $username chat for $reason");
        }
        return $result;
    }
}
