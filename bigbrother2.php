<?php
    require_once("lib/ParseQueryManager.php");
    require_once("lib/smarty/Smarty.class.php");

    session_start();
    if (!isset($_SESSION["parseManager"]) || !isset($_SESSION["user"])) {
        header("Location: index.php");
        die();
    }

    /* @var $queryManager ParseQueryManager */
    $queryManager = $_SESSION["parseManager"];

    //load live broadcasts
    $results = json_decode($queryManager->getCurrentBroadcastingUsers());
    $broadcastingUsers = $results->results;

//    $i = 0;
//    $broadcasts = array();
    foreach ($broadcastingUsers as $user)
    {
//        if ($i >= 4)
//            break;

//        $comments = json_decode($queryManager->getBroadcastComments($user->current_broadcast->objectId));
//        if (isset($comments->results)) {
//            $user->comments = $comments->results;
//        }
//        $i++;
//        array_push($broadcasts, $user->current_broadcast);
        
//          print($user->current_broadcast->objectId);
//        
//          $comments = json_decode($queryManager->getUserByUserID($user->current_broadcast->objectId));
//          $commentsForUser = $result->results;
//          print_r($commentsForUser);
       // var_dump($user->current_broadcast->objectId);
       
        
//        $userObject = $queryManager->getUserByUserID($user->current_broadcast->objectId);
//        print_r($userObject);
    }

    $queryManager->checkFlaggedBroadcasts($broadcasts);

    $smarty = new Smarty();
    $smarty->assign("broadcasts", $broadcastingUsers);
    $smarty->display("templates/bigbrother.tpl");
