<?php

    //********************************************************************************************************
    //adminUserPopupLayout.php - fetches user object data by username and lay out the popup modal structure
    //********************************************************************************************************

    require_once("../lib/ParseQueryManager.php");

    session_start();
    $queryManager = $_SESSION['parseManager'];


    if(isset($_GET['clickchatusername'])) {
        $clickchatusername = $_GET['clickchatusername'];
        //var_dump($clickchatusername);
        
        //retrive user object by username
       $result = $queryManager->getUserByUsername($clickchatusername);
        
        if(!is_null($result)) {
            
            $chatterUserId = $result->objectId;
            $chatterUsername = $result->username;
            $chatterName = $result->name;
            $chatterEmail = $result->email;
            $chatterAvatar1 = $result->avatar->url;
            $chatterAvatar2 = $result->avatar_url;
            $coins = $result->total_coins;
        
            if(isset($chatterAvatar1)) {
                $chatterAvatar = $chatterAvatar1;
            } elseif(isset($chatterAvatar2)) {
                $chatterAvatar = $chatterAvatar2;
            } else {
                $chatterAvatar = "images/defaultavatar.png";
            }

            ?>


        <div class="modal fade" id="modalchatter<?php echo $chatterUsername; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close userModalPopup" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title">
                            <div id='<?php echo $chatterUserId; ?>-modifyUsernameWrapper' class='modifyUsernameWrapper'><p><?php echo $chatterUsername; ?></p></div>                                
                            <button type="button" class="btn btn-info btn-xs modifyUsernameButton" id="<?php echo $chatterUserId; ?>-usernameButton" disabled="disabled" style="display:none;">Update</button>
                        </h3>
                    </div>
                    <div class="modal-body">

                        <input type="hidden" id="disciplineUserId" value="<?php echo $chatterUserId; ?>">
                        <input type="hidden" id="disciplineEmail" value="<?php echo $chatterEmail; ?>">
                        <input type="hidden" id="disciplineUsername" value="<?php echo $chatterUsername; ?>">
<!--                        <input type="hidden" id="disciplineSessionToken" value="{$session_token}">-->

                        <div class="row">
                            <img class="thumbnail-placholder" src="thumb.php?src=<?php echo $chatterAvatar; ?>">
                            
                            <div class="modal-body-content col-md-9">
                                <input type='hidden' id='<?php echo $chatterUserId; ?>-Email' value='<?php echo $chatterEmail; ?>'/>
                                <dl class="dl-horizontal">
                                    <dt>Name</dt><dd><?php echo $chatterName; ?></dd>
                                    <dt>Username</dt><dd><?php echo $chatterUsername; ?></dd>
                                    <dt>Email</dt><dd><?php echo $chatterEmail; ?></dd>
                                    <dt>User ID</dt><dd><?php echo $chatterUserId; ?></dd>
                                    <dt>Total Coins</dt><dd><?php echo $coins; ?> <button type="" class="btn btn-link btn-xs addMoreCoinsTriggerLink">Add More Coins</button></dd>
                                </dl>
                            </div> <!-- end modal-body-content -->
                        </div>
                        
                        <div class="row col-md-12 addCoinsWrapper" style="display: none;">
                            <h5>User Coins</h5>
                            <div class="col-md-2">
                                 <label for="coinAddAmount">Add #</label>
                                <input type="text" class="form-control" id="coinAddAmount"/>
                            </div>
                            <div class="col-md-8">
                                <label for="coinReason">Reason</label>
                                <input type="text" class="form-control" id="coinReason"/>
                                <span class="help-block">Reason is optional.</span>
                            </div>
                            <div class="col-md-2">
                                <button type="" style="position:relative;top:25px;" class="btn btn-success addCoinsButton"><strong>+ Coins</strong></button>
                            </div>
                        </div>

                        <div class="row">
                            <div id="function-button-wrapper">
                                <button type="button" class="btn btn-danger testSuspendUser" id="<?php echo $chatterUserId; ?>">Warn / Suspend / Ban</button>
                                <button type="button" class="btn btn-primary avatarremove" id="<?php echo $chatterUserId; ?>">Remove Avatar</button>
                                <button type="button" class="btn btn-success makefeatured" id="<?php echo $chatterUserId; ?>">Make Featured</button>
                                <button type="button" class="btn btn-warning emailUser" id="<?php echo $chatterEmail; ?>">Email User</button>
                            </div>
                        </div>

                        <!-- empty div that gets populated with email form or suspend actions -->    
                        <div id="<?php echo $chatterUserId; ?>-suspend-wrapper" class="suspend-user-wrapper" style="display:none;"></div>        

                    </div> <!-- /.modal-body -->
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

            <?php

        }
        else {
            return "getUserNameByUsername returned a null result.  Check to see if username is being passed to adminUserPopupLayout.php";
        }
    }
    else {
        return 'Username param is empty.';
    }
   
?>


