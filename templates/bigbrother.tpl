<!DOCTYPE html>
<html lang="en">
<head>
    <title>Hang w/ Live Admin</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    {* stylesheets *}
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="css/custom.css" media="screen"/>
    
    {* scripts *}
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/functions.js"></script>

</head>
<body>

    <nav class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <h3 class="navbar-text">Hang W/ Live Admin</h3>
            </div>
            <div id="nav-status-wrapper">
                <p id="nav-status-message" class="text-primary navbar-text"></p>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#modalhelp" data-toggle="modal"><span class="glyphicon glyphicon-question-sign"></span>  Help</a></li>
                    <li><a href="index.php?logout"><span class="glyphicon glyphicon-off"></span>  Logout</a></li>
                    <li><a id="singlePullLink" href="#"><span class="glyphicon glyphicon-repeat"></span>  Single Pull</a></li>
                    <li><a href="{$smarty.server.PHP_SELF}"><span class="glyphicon glyphicon-refresh"></span>  Refresh All</a></li>
                </ul>
            </div>
        </div>
    </nav>  
                    
    <div id="wrap">
    
    <div class="row"> <!-- row -->
        <div class="col-md-10">
            
            <div id="randomdiv2"></div>
             
            {foreach $broadcasts as $broadcast}
                {if $broadcast@index == 10}
                {break}
                {/if}
                {assign var=userId  value=$broadcast->objectId}
                {assign var=name  value=$broadcast->name}
                {assign var=email  value=$broadcast->email}
                {assign var=username  value=$broadcast->username}
                {assign var=viewer_count  value=$broadcast->current_broadcast->viewer_count}
                {assign var=broadcast_id  value=$broadcast->current_broadcast->objectId}
                {assign var=avatar3  value="images/defaultavatar.png"}
                {assign var=avatar2  value=$broadcast->avatar_url|default:$avatar3}
                {assign var=avatar1  value=$broadcast->avatar->url|default:$avatar2}
                {assign var=viewercount  value=$broadcast->current_broadcast->viewer_count|default:0}
                {assign var=avatar value=$avatar1}
                {assign var=chat_service value=$broadcast->current_broadcast->chat_info->service}
                {assign var=flagged value=$broadcast->current_broadcast->flagged}
                {assign var=private value=$broadcast->private}
                {assign var=verified value=$broadcast->verified}
                {assign var=session_token value=$broadcast->sessionToken}
                {assign var=jabber_id value=strstr($broadcast->current_broadcast->chat_info->room_jid, "@", true)}
                {assign var=broadcast_orientation value=$broadcast->current_broadcast->video_orientation}
                {assign var=coins value=$broadcast->total_coins}

                <div>

                {if $flagged}
                    <div class="phone-flagged" id="holder{$broadcast_id}">
                {elseif $verified == 1}
                    <div class="phone-featured" id="holder{$broadcast_id}">
                {elseif $private == 1 }
                    <div class="phone-private" id="holder{$broadcast_id}">
                {else}
                    <div class="phone" id="holder{$broadcast_id}">
                {/if}
                
                    {* for jabber chat *}
                    <input type="hidden" id="{$broadcast_id}chat-type" value="{$chat_service}"/>
                    <input type="hidden" id="{$broadcast_id}jabber" value="{$jabber_id}"/>
                                      
                    <div class="phone-header"> 
                        <div class="phone-header-avatar" style=""> 
                            <img src="thumb.php?src={$avatar|escape:'url'}" width="50px"/>
                        </div>

                        <div class="phone-header-username-count" style=""> 
                            <strong>{$username}</strong>
                            <br /> 
                            <span><small>Hanging w/ {$viewercount} people</small></span>  
                        </div>

                        <div class="phone-header-close-button" id="{$broadcast_id}"> 
                            <button class="btn btn-xs btn-danger" id="doesntmatter{$broadcast_id}" class="okay"> X </button> 
                        </div>
                    </div>

                        
                {if $broadcast_orientation == 2}
                    {assign var=urlPath value = "http://medl.alldigital.net/rtmplive/{$broadcast_id}/playlist.m3u8"}
                    {assign var=classOrientation value="type2"}
                {elseif $broadcast_orientation == 1}
                    {assign var=urlPath value = "http://medl.alldigital.net/rtmlive/{$broadcast_id}/playlist.m3u8"}
                    {assign var=classOrientation value="type1"}
                {elseif $broadcast_orientation == 0}
                    {assign var=urlPath value = "http://medl.alldigital.net/rtmlive/{$broadcast_id}/playlist.m3u8"}
                    {assign var=classOrientation value="type0"}
                {/if}
                        
                        
                
                <div class="video-wrapper">
                    <input type="hidden" id="orientationTypeHidden" value="{$broadcast_orientation}"/>
                    
                   {if $broadcast_orientation == 2}
                      
                       <video id="{$broadcast_id}" class="videoType2" height="200" width="200" muted autoplay>
                           <source src="http://medl.alldigital.net/rtmplive/{$broadcast_id}/playlist.m3u8" type="application/vnd.apple.mpegurl"/>
                       </video>
                   {elseif $broadcast_orientation == 1}
                      
                       <video id="{$broadcast_id}" class="videoType1" height="200" width="200" muted autoplay>
                           <source src="http://medl.alldigital.net/rtplive/{$broadcast_id}/playlist.m3u8" type="application/vnd.apple.mpegurl"/>
                       </video>

                   {elseif $broadcast_orientation == 0}
                      
                       <video id="{$broadcast_id}" class="videoType0" height="200" width="200" muted autoplay>
                           <source src="http://medl.alldigital.net/rtplive/{$broadcast_id}/playlist.m3u8" type="application/vnd.apple.mpegurl"/>
                       </video>
                   {/if}
                   
                   <div class='video-refresh-wrapper' style='display:none;'>
                       <button type="button" class="btn btn-info btn-xs">
                           <span class="glyphicon glyphicon-refresh"></span>
                       </button>
                   </div>
                </div>

                
                 {* start buttons *}
                 
                <div class="buttons-grouped">
                    <div class="mute-button" id="{$broadcast_id}">
                        <button class="btn btn-warning btn-xs">Mute</button > 
                    </div>

                    <div class="unmute-button" id="{$broadcast_id}">
                        <button class="btn btn-warning btn-xs">Unmute</button > 
                    </div>

                    <div class="update-chat-button" id="{$broadcast_id}"> 
                        <button class="btn btn-primary btn-xs updatechat" id="{$broadcast_id}">Update Chat</button> 
                    </div>
                    
                    <div class="more-button" id="{$broadcast_id}">
                        <a data-toggle="modal" href="#modal{$userId}">
                            <button class="btn btn-success btn-xs">More</button > 
                        </a> 
                    </div>
                </div>

                {* end buttons *}
                
                
                
               {* chat area container *}
                <div class="chatbox" id="chat{$broadcast_id}">
              
                </div> 
                
                                 
{*                <a data-toggle="modal" href="#modal{$userId}" class="btn btn-info btn-xs">New More</a>
*}
                <!-- User Popup Modal -->
                <div class="modal fade" id="modal{$userId}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close userModalPopup" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h3 class="modal-title">
                                    <div id='{$userId}-modifyUsernameWrapper' class='modifyUsernameWrapper'><p>{$username}</p></div>                                
                                    <button type="button" class="btn btn-info btn-xs modifyUsernameButton" id="{$userId}-usernameButton" disabled="disabled" style="display:none;">Update</button>
                                </h3>
                            </div>
                            <div class="modal-body">
                                
                               <input type="hidden" id="disciplineUserId" value="{$userId}">
                               <input type="hidden" id="disciplineEmail" value="{$email}">
                               <input type="hidden" id="disciplineBroadcastId" value="{$broadcast_id}">
                               <input type="hidden" id="disciplineUsername" value="{$username}">
                               <input type="hidden" id="disciplineSessionToken" value="{$session_token}">
                                
                                <div class="row">
                                    <img src="thumb.php?src={$avatar|escape:'url'}">
                                    
                                    <div class="modal-body-content col-md-9">
                                        <input type='hidden' id='{$userId}-Email' value='{$email}'/>
                                        <dl class="dl-horizontal">
                                            <dt>Name</dt><dd>{$name}</dd>
                                            <dt>Username</dt><dd>{$username}</dd>
                                            <dt>Email</dt><dd>{$email}</dd>
                                            <dt>User ID</dt><dd>{$userId}</dd>
                                            <dt>Broadcast ID</dt><dd>{$broadcast_id}</dd>
                                            <dt>Total Coins</dt><dd>{$coins} <button type="" class="btn btn-link btn-xs addMoreCoinsTriggerLink">Add More Coins</button></dd>
                                        </dl>
                                    </div> <!-- end modal-body-content -->
                                </div>
                                
                                <div class="row col-md-12 addCoinsWrapper" style="display: none;">
                                    <h5>User Coins</h5>
                                    <div class="col-md-2">
                                         <label for="coinAddAmount">Add #</label>
                                        <input type="text" class="form-control" id="coinAddAmount"/>
                                    </div>
                                    <div class="col-md-8">
                                        <label for="coinReason">Reason</label>
                                        <input type="text" class="form-control" id="coinReason"/>
                                        <span class="help-block">Reason is optional.</span>
                                    </div>
                                    <div class="col-md-2">
                                        <button type="" style="position:relative;top:25px;" class="btn btn-success addCoinsButton"><strong>+ Coins</strong></button>
                                    </div>
                                </div>
                                        
                                <div class="row">
                                    <div id="function-button-wrapper">
                                        <button type="button" class="btn btn-danger testSuspendUser" id="{$userId}">Warn / Suspend / Ban</button>
                                        <button type="button" class="btn btn-primary avatarremove" id="{$userId}">Remove Avatar</button>
                                        <button type="button" class="btn btn-success makefeatured" id="{$userId}">Make Featured</button>
                                        <button type="button" class="btn btn-warning emailUser" id="{$email}">Email User</button>
      {*                                  <button type="button" class="btn btn-default testSuspendUser" id="{$userId}">Test Suspend</button>*}
                                    </div>
                                </div>
                                        
                                <!-- empty div that gets populated with email form or suspend actions -->    
                                <div id="{$userId}-suspend-wrapper" class="suspend-user-wrapper" style="display:none;"></div>        

                            </div> <!-- /.modal-body -->
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                
                <script>

                    //auto-load comments - trigger on update-button click
                    //set for 5 seconds after div has been loaded into DOM
                    var timer;
                    
                    $(document).ready(function() {
                        if(timer) {
                            clearTimeout(timer);
                        }
                        timer = window.setTimeout(function() {
                            $('.update-chat-button').trigger('click', function() {
                                console.log('updating chat for ' + $(this).attr('id'));
                            });
                        }, 3000);
                    });

                </script>
               
                
                </div> {* end phone *}
             </div>
                
             {/foreach} {* end broadcasts *}

        </div> {* end col-md-10 *}
            
            
        
        
        <div class="col-md-2" id="sidebar">
           
            {* *** Load in refreshsidebar.php on load *}
            <script>
                            
            $.ajax({
                url: "refreshbroadcasters.php",
                type: "GET",
                timeout: 3000,
                beforeSend: function() {
                    $('#sidebar').html("<div id='sidebar-title'>Live Broadcasters</div><p class='text-center text-info'>Loading Broadcasters...</p>");
                },
                success: function(result) {
                    $('#sidebar').empty().html(result);
                }
            });

            </script>
           
        </div> {*end sidebar *}
            
    </div> {* end row *}
    
    
    <div class="modal fade" id="modalhelp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modalhelp">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Hang w/ Live Admin Help</h4>
                </div>
                <div class="modal-body">
                    <h4>Basics</h4>
                    <dl class="dl-horizontal">
                        <dt>Single Pull</dt>
                        <dd>Click to pull a random broadcast <strong>if and only if</strong> the broadcast isn't currently being shown.</dd>
                        <dt>Refresh All</dt>
                        <dd>Click to refresh all current broadcasts including the sidebar.</dd>
                        <dt>Mute/Unmute Buttons</dt>
                        <dd>Mute and Unmute the volume of a broadcast, respectively.</dd>
                        <dt>Update Chat</dt>
                        <dd>Click to update the chat log for a corresponding broadcast.</dd>
                        <dt>More Button</dt>
                        <dd>Click to access admin tasks for a broadcast: Warn/Suspend/Ban, Remove Avatar, Make Featured, & Email Broadcaster.</dd>
                        <dt>Close Broadcast (X)</dt>
                        <dd>Click to close broadcast, will auto load another broadcast from right sidebar <strong>if and only if</strong> the broadcast isn't currently being shown.</dd>
                    </dl>
                    <h4>Tips</h4>
                    <ul>
                        <li>Right sidebar displays all current, live broadcasters and refreshes often. Clicking an avatar will launch its broadcast, but only if it's new/not currenlty being shown.</li>
                        <li>Clicking on the username of a user in the chat log will launch the admin tasks popup for that user similar to the 'More' button.</li>
                        <li>If chat log shows 'Update Chat Timeout', the update chat request took too long. Clicking on 'Update Chat' will refresh the chat log.</li>
                        <li>Some broadcasts will show a blank, white screen despite having the video source loaded.  Clicking on the video container will display a 'Refresh' button that will reload the video for that broadcasts.</li>
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
    
    <div class="push"></div>
    </div> <!-- wrap -->
    
    <footer>
        <p class="text-center text-muted">&copy; Hang W/ {$smarty.now|date_format:"%Y"}</p>
    </footer>

</body>
</html>