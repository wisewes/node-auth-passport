<?php 

/********************************************************************************************
 * Gets called when Update Chat cliked for broadcaster; 
 * Fetches jabber chat log and formats it. 
 * THe results from this file are inserted into the chatbox div for the broadcaster
 *********************************************************************************************/

require_once("lib/ParseQueryManager.php");

session_start();
$queryManager = $_SESSION['parseManager'];


if(isset($_GET['jabberid'])) {
    
    $jabber_id = $_GET['jabberid'];
    
    //TEST Path - http://www.hangwith.com/bigbrother/mucChat.php?mucId=ios_test
    $path = "http://www.hangwith.com/bigbrother/mucChat.php?mucId=" . $jabber_id;

    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $path);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, '30');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    $content = trim(curl_exec($ch));
    curl_close($ch);

    //format the result
    $matches = "";
    preg_match_all("/{(.*?)}/", $content, $matches);
    $jsonMessages = $matches[0];
   
    
    
    if(empty($content)) {
        print "<p class='text-danger'><strong>No one is chatting.</strong></p>";
    }
    else {
        
        foreach ($jsonMessages as $jsm) {
            $output = json_decode($jsm, true);
            
            //ouptut chat log structure: [usenrame : message]
            $usernameLink = "<a data-toggle='modal' href='#modalchatter{$output['user']}' class='chatterUserFetch'>{$output['user']}</a>";
            $chatOutput = " : {$output['msg']}</br>";
            print $usernameLink . $chatOutput;
        }
    }
}
else {
    print "<p class='text-primary'><strong>Jabber Room Unknown.</strong></p>";
}



//**** PARSE WORKING IMPLEMENTATION - below is the code necessary to get chats working via Parse
// LEGACY - left in as reference in case needed in future
//$ch = curl_init();
//curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Parse-Application-Id: KiFaNKo92CrkhgLiqD6BwgoQCjjj7mbV7redGUVv', 'X-Parse-Master-Key: 8ARUanFVQuMjzqy4lCLT1Fo8JgJmeHFpUSuhAohK'));
//curl_setopt($ch, CURLOPT_URL, "https://api.parse.com/1/classes/Comment?where=%7B%22%24relatedTo%22%3A%7B%22object%22%3A%7B%22__type%22%3A%22Pointer%22%2C%22className%22%3A%22Broadcast%22%2C%22objectId%22%3A%22" . $broadcast_id . "%22%7D%2C%22key%22%3A%22comments%22%7D%7D&include=user");
//curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//curl_setopt($ch, CURLOPT_TIMEOUT, '30');
//curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
//$content = trim(curl_exec($ch));
//curl_close($ch);
//
//$json_decoded = json_decode($content);
//$jsonarray = $json_decoded->results;

//preg_match_all("/{(.*?)}/", $content2, $matches);
//$jsonMessages = $matches[0];


//foreach($jsonarray as $jsonMsg) 
//{
//       // $chat = $jsonMsg->body;        
//        $namechat = $jsonMsg->user->name;
//        $usernamechat = $jsonMsg->user->username;
//        $emailchat = $jsonMsg->user->email;
//        $useridchat = $jsonMsg->user->objectId;
//        $timestamp = $jsonMsg->user->createdAt;
//        
//        $avatarchat1 = $jsonMsg->user->avatar->url;
//        $avatarchat2 = $jsonMsg->user->avatar_url;
//        
//        if(isset($avatarchat1)) {
//            $avatar = $avatarchat1;
//        } elseif(isset($avatarchat2)) {
//            $avatar = $avatarchat2;
//        } else {
//            $avatar = "images/defaultavatar.png";
//        }
                
	//$msgChunks = json_decode($jsonMsg);
//	$chat = $msgChunks->msg;
//	$usernamechat = $msgChunks->user;
//	$timeStamp = $msgChunks->timeStamp;
//	$namechat = $jsonarrays2->user->name;
//	$emailchat = $jsonarrays2->user->email;
//	$avatar1chat = $jsonarrays2->user->avatar->url;
//	$avatar2chat = $jsonarrays2->user->avatar_url;
//	$avatar3 = "images/defaultavatar.png";
//	$useridchat = $jsonarrays2->user->objectId;
//}
?>




