/******
 *  
 *  Functions for managing users/broadcasts -
 *  removed from inline scripts from bigbrother2.php
 * 
 *  The following functions have tweaks to allow functionality to work
 *  with new divs for styling
 *  
 ******/

(function ($) {
     
    $(document).ready(function () {
        
        //constants
        var CONSTANTS = {
           SIDEBAR_REFRESH_TIMEOUT : 8000,
           MESSAGE_STATUS_TIMEOUT : 10000,
           PARSE_PRODUCTION_APP_KEY: "KiFaNKo92CrkhgLiqD6BwgoQCjjj7mbV7redGUVv",
           PARSE_PRODUCTION_REST_API_KEY: "IrfKLdKZVPne96erlQcBHm5FI0qrl8j8sMjrFvL0",
           PARSE_DEVELOPMENT_APP_KEY: "oMRHUbuu5Q6Wdm5ZVo5vypOQi8x5fwJK7rBS9KOs",
           PARSE_DEVELOPMENT_REST_API_KEY: "6Dxhffd009MCpE8L8cxm6HhCa1U7PPxI4v9LTCwY"
        };
        
        
        //variables for commonly-used selectors
        var statusMessage = $("#nav-status-message");
        
        //*************************
        // Defined Functions
        ///************************
        
        function sidebarRefresh() {
            
            //load refresh broadcasters output into sidebar
            $.ajax({
                url: "refreshbroadcasters.php",
                type: "GET",
                timeout: 5000,
                beforeSend: function() {
                    $('#sidebar').html("<div id='sidebar-title'>Live Broadcasters</div><p class='text-center text-info'>Refreshing Broadcasters...</p>");
                },
                success: function(result) {
                    $('#sidebar').empty().html(result);
                    
                    //check for stale broadcasts and highlight them
                    highlightStaleBroadcasts();
                },
                error: function(error) {
                    console.log('sidebar refresh timed out');
                }
            });
        }
    
    
        //HIGHLIGHT STALE BROADCASTS AFTER BROADCAST HAS FINSIHED 
        function highlightStaleBroadcasts() {
             
            var inSidebar = [];
            var inBroadcasters = [];
            
            //get ids for live broadcasters
            $('#sidebar').find('.specificpull').each(function(index) {
                inSidebar.push($(this).attr('id'));
            });
            console.log('refresh: sidebar' + inSidebar);
    
            //get ids for showing broadcasters
            $('.phone, .phone-featured, .phone-flagged, .phone-private').each(function(index) {
                inBroadcasters.push($(this).find('#disciplineUserId').val());
            });
            console.log('refresh: broadcasts ' + inBroadcasters);
            
            var unique = $(inBroadcasters).not(inSidebar).get();
            console.log('refresh: unique = ' + unique);
            
            if (unique) {
                $.each(unique, function(i, val) {
                    $('#modal' + val).parents('.phone, .phone-featured, .phone-flagged, .phone-private').delay(30000).css('box-shadow', '1px 1px 10px black');
                });
            }
        }
    
    
        //**************************************************************************
        // remove email injected div from when popup modal close button is clicked
        //**************************************************************************
        function removeEmailDiv() {
            $('.suspend-user-wrapper').empty();
            $('.emailUser').prop('disabled', false);
        }



        //*************************************************
        // Auto-refresh for all broadcasters in sidebar
        //*************************************************
        
        window.setInterval(function() {
            sidebarRefresh();
        }, CONSTANTS.SIDEBAR_REFRESH_TIMEOUT); //refresh every 5 seconds


        //******************************************************
        // Direct Selectors/functionality on broadcast divs
        //******************************************************
        
        //sidebar button
        $("#sidebar-toggle-button").on('click', function() {
           
           if($('#sidebar').is(':hidden')) {
               $('#sidebar').slideDown();
               $('#sidebar').show();
               $('#sidebar-toggle-button').removeClass('btn-info').addClass('btn-default');
           }
           else {
               $('#sidebar').slideUp();
               $("#sidebar").hide();
               $('#sidebar-toggle-button').addClass('btn-info').removeClass('btn-default');
           }
        });
    
    
        //*****************************************************************
        // Close broadcast (X) button on broadcasting outline div
        //*****************************************************************
        
        $('body').on('click', '.phone-header-close-button', function() {
            
            $(this).parents('.phone, .phone-featured, .phone-flagged, .phone-private').parent().remove();
            var broadcasterToClose = $(this).parents('.phone, .phone-featured, .phone-flagged, .phone-private').find('#disciplineUserId').val();
         
            console.log('CLOSE ON ID = ' + broadcasterToClose);
         
            var showingBroadcasts = [];
            var listOfBroadcasters = [];
            
            //create array of ids for showing broadcasts exluding the one being closed
            $('.phone, .phone-featured, .phone-flagged, .phone-private').each(function(index) {
                
                if($(this).find('#disciplineUserId').val() !== broadcasterToClose) {
                    showingBroadcasts.push($(this).find('#disciplineUserId').val());
                }
            });
            
            //create array of ids for live braodcasters in sidebar exluding the one being closed
            $("#sidebar-wrapper").find('.specificpull').each(function(index) {
                
                if($(this).attr('id') !== broadcasterToClose) {
                    listOfBroadcasters.push($(this).attr('id'));    
                }
            });
            
            console.log('broadcats: ' + showingBroadcasts);
            console.log('sidebar items = ' + listOfBroadcasters);
    
            //create arary of unique values between the above two arrays
            var unique = $(listOfBroadcasters).not(showingBroadcasts).get();
            console.log('unique = ' + unique);
            
            //randomly select a value from the unique array
            var randomValue = unique[Math.floor(Math.random() * unique.length)];
    
            //check to see if user is still a broadcaster
            if($.inArray(randomValue, listOfBroadcasters) >= 0) {
                
                console.log("randomValue: " + randomValue + " is in list of broadcasters");
                
                $(statusMessage).html('Fetching Broadcast...').fadeIn();
                $("#randomdiv2").prepend($("<div>").load("showbroadcast.php?specificbroadcast=" + randomValue, function() {
    
                    $(statusMessage).fadeOut().html("");
                }));
            }
        });


        //click on make feature button - features a broadcast
        $('body').on('click', '.makefeatured', function() {
            var makefeatured = $(this).attr('id');
            $.post("testput.php?relationadd=" + makefeatured, function(data) {
    //alert("User Added");
                alert(data);
            });
        });


        // click on mute button
        $('body').on('click', '.mute-button', function() {
            var video = $(this).attr('id');        
            var videoObject = $('video#' + video);
    
            videoObject.prop('muted', true);
        });
    
    
        // click on unmute
        $('body').on('click', '.unmute-button', function() {
            var video = $(this).attr('id');
            var videoObject = $('video#' + video);
            
            videoObject.prop('muted', false);
        });
    


        //SINGLE PULL  - adds a broadcast to the view view
        $('body').on('click', '#singlePullLink', function() {
            
            var sidebarBroadcasters = [];
            var currentShowing = [];
           
            //get ids for live broadcasters
            $('#sidebar').find('.specificpull').each(function(index) {
                sidebarBroadcasters.push($(this).attr('id'));
            });
            console.log('sidebar broadcasts are: ' + sidebarBroadcasters);
    
            //get ids for showing broadcasters
            $(".phone").each(function(index) {
                currentShowing.push($(this).find('#disciplineUserId').val());
            });
            console.log('currently showing are: ' + currentShowing);
    
            //get unique array from values not intersected in the two arrays
            var unique = $(sidebarBroadcasters).not(currentShowing).get();
            console.log('unique = ' + unique);
    
            if (unique.length === 0) {
                //console.log('unique array is empty');
                $(statusMessage).html('ALL AVAILABLE BROADCASTS ARE SHOWING').fadeIn();
    
                window.setInterval(function() {
                    $(statusMessage).fadeOut().html('');
                }, CONSTANTS.MESSAGE_STATUS_TIMEOUT);
            }
            else {
                $(statusMessage).html('Fetching Broadcast...').fadeIn();
    
                $('#randomdiv2').prepend($("<div>").load("showbroadcast.php?specificbroadcast=" + unique[0], function() {
                    $(statusMessage).fadeOut().html("");
                }));
            }
        });



        //click on specific user avatar - adds a specific broadcast to view
        $('body').on('click', '.specificpull', function() {
    
            var alreadyShowingBroadcasts = [];
            var specificbroadcast = $(this).attr('id');
            console.log('specificbroadcast = ' + specificbroadcast);
    
            //ensure that broadcast isn't already showing; if not, fetch it
            if(!checkIfBroadcastIsShowing(specificbroadcast)) {
                $(statusMessage).html('Fetching Broadcast...').fadeIn();
    
                $('#randomdiv2').prepend($("<div>").load("showbroadcast.php?specificbroadcast=" + specificbroadcast, function() {
                    $(statusMessage).fadeOut().html("");
                })); 
            } 
        });
    
        //helper function for checking if a selected userid is currently broadcasting
        function checkIfBroadcastIsShowing(broadcastToCheck) {
            
            var checker = null;
            var alreadyShowing = [];
            
            $('.phone, .phone-featured, .phone-flagged, .phone-private').each(function(index) {
                console.log(index + ": " + $(this).find('#disciplineUserId').val());
                alreadyShowing.push($(this).find('#disciplineUserId').val());
            });
            
            //check if userid for specificpull is currently shown \
            // $.inArray: any integer >= 0 is the index of the found value
            if($.inArray(broadcastToCheck, alreadyShowing) >= 0) {
                checker = true;
                console.log('new broadcast: ' + broadcastToCheck + ' was found in array - no pull');
            }
            else {
                checker = false;
                console.log('new broadcast: ' + broadcastToCheck + ' was NOT found in array');
            }
            
            return checker;
        }

    
        //*****************************************************************
        // CLICK ON UPDATE CHAT BUTTON - refreshes current broadcast chatlog
        //*****************************************************************
        $('body').on('click', '.update-chat-button', function() {
            var currentId = $(this).attr('id');
            var chatType = $("#" + currentId + "chat-type").val();
            var jabberId = $("#" + currentId + "jabber").val();
            
            if(!jabberId) {
                $('#chat' + currentId).empty().append("<div><p class='text-warning'><strong>No Jabber ID.</strong></p></div>");
            }else {
                $.ajax({
                    url: "updatechat.php",
                    type: "GET",
                    data: {updatechat: currentId, chattype: chatType, jabberid: jabberId},
                    beforeSend: function() {
                        $(statusMessage).html('Updating Chat...').fadeIn();
                        $('#chat' + currentId).empty().append("<div><p class='text-warning'><strong>Updating Chat...</strong></p></div>");
                    },
                    success: function(result) {
    
                        $("#chat" + currentId).empty().append($("<div id='chatList'>").html(result));
                    },
                    error: function(error) {
                        //will get displayed if this ajax call times out
                        $('#chat' + currentId).empty().append("<div><p class='text-warning'><strong>Update Chat Timeout.</strong></p></div>");
    
                    },
                    complete: function() {
                        $(statusMessage).fadeOut().html("");
                    }
                });
            }
            //fetch the broadcast's chat log
        });
    
    
    
        //********************************************************************************************
        // CLICK ON CHATTER USERNAME - fetch Parse object data and load modal window for admin tasks
        //******************************************************************************************** 
       $('body').on('click', '.chatterUserFetch', function() {
          
            var usernameToPass = $(this).text();
            var injectContainer = $(this).parents('.chatbox');
            var sanitizedUsernameSelector = "";
            
            //***** sanitize the username to be added with a selector
            //jquery selectors break if they contain special chars such as a . or a space at the end
            //selector in form of: #modalchatter{username}
            if(usernameToPass.indexOf('.') !== -1) {
                sanitizedUsernameSelector = usernameToPass.replace(/\./g, "\\.");
            }
            else if(usernameToPass.indexOf(' ') !== -1) {
                sanitizedUsernameSelector = usernameToPass.replace(/\s/g, "\\s");
            }
            else if(usernameToPass.indexOf('~') !== -1) {
                sanitizedUsernameSelector = usernameToPass.replace(/\~/g, "\\~");
            }
            else {
                sanitizedUsernameSelector = usernameToPass;
            }
             
            var chatterModalID = '#modalchatter' + sanitizedUsernameSelector;
    
            console.log('chatterUsername = ' + usernameToPass);
            
            if($(chatterModalID).length) {
    
                $(chatterModalID).modal('show');
                
            } else {
                
                //fire off parse request to get chatting user object data
                $.ajax({
                    url: "templates/adminUserPopupLayout.php",
                    type: "GET",
                    data: {clickchatusername: usernameToPass},
                    beforeSend: function() {
                        $(statusMessage).html("Looking up username...").fadeIn();
                    },
                    success: function(result) {
                        
                        if (result) {
                            
                           $(injectContainer).find('#chatList').append(result);
                           $(chatterModalID).modal('show');
                            
                        } else {
                            alert('Ajax request failed for adminUserPopuplayout; result empty');
                        } 
                    },
                    error: function(error) {
                        alert('Unable to retrieve chatting user data object: ' + JSON.stringify(error));
                    },
                    complete: function() {
                        $(statusMessage).html("").fadeOut();
                    }
                });
            }
       });
    
    
    
    //********************************************************************************************
    // The following two functions work together:
        // Clicking on the video div will display a refresh button, clicking on the refresh button
        // will reload the <video><source></source></video> tag structure
        //******************************************************************************************** 
        
        //display refresh button when video div is clicked
        $('body').on('click', 'video', function() {
            var id = $(this).attr('id');
            var refreshWrapper = $(this).parent().find('.video-refresh-wrapper');
            
            $(refreshWrapper).fadeIn();
        });
        
        //reload video wrapper with contents to refresh video
        $('body').on('click', '.video-refresh-wrapper', function() {
                   
            var videoWrapperDiv = $(this).parents('.video-wrapper').html();
            $(this).parents('.video-wrapper').empty().append(videoWrapperDiv);
        });
        
    
   
        //********************************************************************************************
        // CLICK ON USERNAME TO CHANGE - change username and submit email on success
        //******************************************************************************************** 
        $('body').on('click', '.modifyUsernameWrapper', function() {
            
            //commonly used selectors
            var currentUsername = $(this).text();
            var userId = $(this).parents('.modal-dialog').find('#disciplineUserId').val();
            var sessionToken = $(this).parents('.modal-dialog').find('#disciplineSessionToken').val();
            var userEmail = $(this).parents('.modal-dialog').find('#disciplineEmail').val();
            var usernameWrapper = $('#' + userId + '-modifyUsernameWrapper');
            var usernameButton = $('#' + userId + '-usernameButton');
            var inputBox = "<input type='text' style='position:relative;bottom:5px;' class='form-control inputBox' placeholder=\"New Username\" value=\"" + $(this).text() + "\">";
           
            if($(this).children('input').length === 0) {
                
                $(this).html(inputBox);
                
                $("input.inputBox").focus();
                $("input.inputBox").blur(function() {
                   
                    var usernameNew = $(this).val();
                    $(usernameWrapper).empty().html("<p>" + usernameNew + "</p>");
                    
                    //enable button
                    $('#' + userId + '-usernameButton').text('Update').prop('disabled', false).show();
                    
                    //respond to clicking on updte button - ajax to update username
                    $(usernameButton).click(function(e) {
                       
                         $.ajax({
                            type: "PUT",
                            dataType: "json",
                            contentType: "application/json",
                            url: "https://api.parse.com/1/users/" + userId,
                            headers: {'X-Parse-Application-Id' : CONSTANTS.PARSE_PRODUCTION_APP_KEY, 
                                      'X-Parse-REST-API-Key' : CONSTANTS.PARSE_PRODUCTION_REST_API_KEY, 
                                      'X-Parse-Session-Token' : sessionToken},
                            data: JSON.stringify({
                                "username": usernameNew
                            }),
                            success: function(data) {
                                console.log(JSON.stringify(data));
                                
                                //disable button and prevent clicking to change username again
                                $(usernameButton).html('Changed').prop('disabled', true);
                                $(usernameWrapper).click(false);
                                
                                //fire off email to user about change
                                $.ajax({
                                    url: "emailuserAboutUsernameChange.php",
                                    data: {oldusername: currentUsername, newusername: usernameNew, useremailaddress: userEmail},
                                    type: "GET",
                                    success: function() {
                                        $(statusMessage).html('Changed Username and sent email to ' + userEmail).fadeIn(function() {
                                            setTimeout(function() {
                                                $(statusMessage).fadeOut().html("");
                                            }, CONSTANTS.MESSAGE_STATUS_TIMEOUT);
                                        });
                                        
                                        alert('Username has been updated and email has been sent to ' + userEmail + '.');
                                    },
                                    error: function() {
                                        
                                    }
                                });
                            },
                            error: function() {
                        
                            }
                        });
                    });
                });
            }  //end if 
        });
    
    
    
    
        //*************************************
        //  Suspend User - updated functionality
        //  
        //***************************************
        
        $('body').on('click', '.testSuspendUser', function() {
            var userId = $(this).attr('id');
            //var username = "blah";
            //var broadcastId = "testbroadcastid";
           
            $('#' + userId + '-suspend-wrapper').fadeIn(function() {
             
                //load the discplinary html markup and append it to to below the function buttons
                $.ajax({
                    url: "templates/suspendUserLayout.php",
                    data: { userId: userId },
                    type: "GET",
                    success: function(result) {
    
                        $('#' + userId + '-suspend-wrapper').empty().append($('<div>')).html(result);
                    
                        //respond to suspend reason change
                        $('select#suspendReason').change(function() {
                            //get selected reason and set placeholder and focus textfield
                            var selectedOption = $("#suspendReason option:selected").text();
    
                            if (selectedOption === 'Other') {
                                $('fieldset').prop('disabled', false);
                                $('#otherReason').focus().prop('placeholder', 'Reason for suspension');
                            }
                            else {
                                $('fieldset').prop('disabled', true);
                                $('#otherReason').blur().prop('placeholder', '');
                            }
                        });
        
        
                        //respond to displinary action select box
                        $('select#suspendSelection').change(function() {
    
                            //buttons
                            var buttonKill = $('#buttonKill-' + userId);
                            var buttonWarn = $('#buttonWarn-' + userId);
                            var buttonSuspend = $('#buttonSuspend-' + userId);
                            var buttonBan = $('#buttonBan-' + userId);
    
                            //get selected action and display textarea for message
                            var selectedBlockOption = $('#suspendSelection option:selected').text();
                            //console.log("selected option = " + selectedBlockOption);
                            
                            //kill
                            if(selectedBlockOption === 'Kill Broadcast') {
                                $('.suspendReasoningWrapper').fadeOut();
                                $('.durationWrapper').fadeOut();
                                $('.suspendButtonsWrapper').find('button').hide();
                                $(buttonKill).show();
                            }
                            
                            //warn
                            if (selectedBlockOption === 'Warn') {
                                $('.warnUserWrapper').fadeIn().focus();
                                $('textarea#warnUserInput').focus();
                                $('.suspendReasoningWrapper').fadeIn();
                                $('.durationWrapper').fadeOut();
                                $('div.otherField').hide();
                                $('.suspendButtonsWrapper').find('button').hide();
                                $(buttonWarn).show();
                            }
                            else {
                                $('.warnUserWrapper').hide().blur();
                            }
                            
                            //suspend
                            if (selectedBlockOption === 'Suspend') {
                                $('.suspendReasoningWrapper').fadeIn();
                                $('.durationWrapper').fadeIn();
                                $('div.otherField').show();
                                $('.suspendButtonsWrapper').find('button').hide();
                                $(buttonSuspend).show();
                            }
                            
                            //ban
                            if(selectedBlockOption === 'Ban') {
                                $('.suspendReasoningWrapper').fadeIn();
                                $('div.otherField').show();
                                $('.durationWrapper').fadeOut();
                                $('.suspendButtonsWrapper').find('button').hide();
                                $(buttonBan).show();
                            }
                            
                            ////make private *** temp fix for banning
                            //if(selectedBlockOption === 'Make Private') {
                            //    $('.suspendButtonsWrapper').find('button').hide();
                            //    $('.suspendButtonsWrapper').prepend('<p class="text-info">*** NOTE <strong>Make Private</strong> will make all broadcasts private so they will not appear in Broadcasting Now.</p>');
                            //    $(buttonBan).text('Make Private').show();
                            //}
                        });
                    },
                    error: function() {
    
                    }
                });
           });
        });
    
    
        //*********************************************************************************
        // CORE DISCIPLINE BUTTONS - fetch appropriate ids from hidden inputs in modal-body
        //*********************************************************************************

        // CLICK ON KILL BROADCAST
        $('body').on('click', "button[id^='buttonKill-']", function() {
            
            var broadcasterToKill = $(this).parents('.modal-body').find('#disciplineUserId').val();
            var broadcastID = $(this).parents('.modal-body').find('#disciplineBroadcastId').val();
            console.log('id:' + broadcasterToKill);
    
            $.post("testput.php?userbroadcasting=" + broadcasterToKill, function(data) {
                console.log('broadcast killed: ' + data);
                
                var videoWrapper = $('video#' + broadcastID).parent();
                $(videoWrapper).empty().html("<p class='text-danger' style='position:relative;top:115px;text-align:center;'>Killed Broadcast</p>");
                
                $(statusMessage).html('Killed broadcast with id: ' + broadcasterToKill).fadeIn(function() {
                    setTimeout(function() {
                        $(statusMessage).fadeOut().html("");
                    }, CONSTANTS.MESSAGE_STATUS_TIMEOUT);
                });
                
            });
        });
    
    
        // CLICK ON WARN
        $('body').on('click', "button[id^='buttonWarn-']", function() {
            
            var userToWarn = $(this).parents('.modal-body').find('#disciplineUserId').val();
            var usernameToWarn = $(this).parents('.modal-body').find('#disciplineUsername').val();
            var warnReason = $('#suspendReason option:selected').text();
            var warnMessage = $('textarea#warnUserInput').html();
            
            var formattedMessage = "[" + warnReason + "] " + warnMessage; 
            
            var encodedString = 'disciplineactions.php?actionvalue=warn&username=' + usernameToWarn + '&userid=' + userToWarn + '&reason=' + formattedMessage;
            console.log(encodedString);
            
            $.ajax({
                type: "GET",
                url: encodedString,
                data: { actionvalue: 'warn', username: usernameToWarn, userid: userToWarn, reason: formattedMessage },
                success: function(result) {
                    alert('User ' + usernameToWarn + ' (' + userToWarn + ') has been warned for ' + formattedMessage);
                },
                error: function(error) {
                    alert(error);
                }
            });
        });
        
        
        // CLICK FOR SUSPEND
        $('body').on('click', "button[id^='buttonSuspend-']", function() {
            
            var userToSuspend = $(this).parents('.modal-body').find('#disciplineUserId').val();
            var usernameToSuspend = $(this).parents('.modal-body').find('#disciplineUsername').val();
            var suspendReason = $("#suspendReason option:selected").text();
            var durationPeriod = $("#duration option:selected").val();
            var message = $('input#otherReason').html();
            
            var encodedString = "";
            
            if(suspendReason === "Other") {
            
                encodedString = "disciplineactions.php?actionvalue=suspend&username=" + usernameToSuspend + "&userid=" + userToSuspend + "&duration=" + durationPeriod + "&reason=" + suspendReason + " : " + message;         
            }   
            else {
                encodedString = "disciplineactions.php?actionvalue=suspend&username=" + usernameToSuspend + "&userid=" + userToSuspend + "&duration=" + durationPeriod + "&reason=" + suspendReason;
            }
            
            console.log(encodedString);
          
            $.ajax({
                type: "GET",
                url: encodedString,
                success: function(result) {
                    alert('User ' + usernameToSuspend + ' (' + userToSuspend + ') has been suspended for ' + suspendReason + ' for ' + durationPeriod + ' hrs.');
                },
                complete: function(result) {
                    
                },
                error: function(error) {
                    alert(error);
                }
            });
        });
    
    
        // CLICK ON BAN 
        $('body').on('click', "button[id^='buttonBan-']", function() {
    
            var userToBan = $(this).parents('.modal-body').find('#disciplineUserId').val();
            var usernameToBan = $(this).parents('.modal-body').find('#disciplineUsername').val();
            var banReason = $("#suspendReason option:selected").text();
            var message = $('input#otherReason').html();
            
            //alert('userToBan:' + userToBan + ', usernameToBan: ' + usernameToBan + ', reason = ' + banReason);
            
            var encodedString = '';
            
            if (banReason === "Other") {
                encodedString = "disciplineactions.php?actionvalue=ban&username=" + usernameToBan + "&userid=" + userToBan + "&reason=" + banReason + " : " + message;
            } else {
                encodedString = "disciplineactions.php?actionvalue=ban&username=" + usernameToBan + "&userid=" + userToBan + "&reason=" + banReason;
            }
    
            //var banMe = 'vqQQKy5x6w';
            //encodedString = "disciplineactions.php?actionvalue=ban&userid=" + userToBan;
            console.log(encodedString);
            
            var messageText = "Are you sure that you want to ban this user " + usernameToBan + "?\n";
            messageText += "Click [ OK ] to BAN this user.\n";
            messageText += "NOTE: Banning will prevent a user from broadcasting.";
            
            var confirmResult = confirm(messageText);
            
            if(confirmResult) {
                
                //fire off ajax call to disciplneActions once moderator confirms banning of user
                $.ajax({
                    type: "GET",
                    url: encodedString,
                    success: function(result) {
                        
                        alert('User with username ' + usernameToBan + ' has been banned.');
                            
                        $(statusMessage).html("user with username " + usernameToBan + " has been banned.").fadeIn(function() {
                           setTimeout(function() {
                            $(statusMessage).fadeOut().html("");
                            
                           }, CONSTANTS.MESSAGE_STATUS_TIMEOUT);
                        });
                    },
                    error: function(error) {
                        alert('Unable to ban user because ' + JSON.stringify(error));
                    }
                });
            } else {
               
            }
        });
    
    
        /**********************************************************************************
         * Aux functions - update dom with values entered in text fields/inputs
         **********************************************************************************/
        
        $('body').on('keyup', 'textarea#warnUserInput', function() {
            var inputValue = $(this).val();
            console.log(inputValue);
            $('textarea#warnUserInput').html(inputValue);
        });
        
        $('body').on('keyup', 'input#otherReason', function() {
           
            var inputValue = $(this).val();
            console.log(inputValue);
            $('input#otherReason').html(inputValue);
        });
    



    
        //****************************************************************************
        // BROADCASTER ADMIN POPUP - FUNCTION BUTTONS
        //****************************************************************************
        
    
        //*** COIN MANAGEMENT

        //display div for adding coins when link is clicked
        $('body').on('click', '.addMoreCoinsTriggerLink', function() {
            $('.addCoinsWrapper').fadeIn();        
        });
        
        //send off coin data to addCoins.php
        $('body').on('click', 'button.addCoinsButton', function() {
            
            var userId = $(this).parents('.modal-body').find('#disciplineUserId').val();
            
            var coinAmount = $(this).parents('.addCoinsWrapper').find('#coinAddAmount').val();
            var coinReason = $(this).parents('.addCoinsWrapper').find('#coinReason').val();
            
            if (isNaN(coinAmount) || coinAmount === '') {
            
                alert('Add # value must be a whole number.');
            } else {
                
                $.ajax({
                    type: "POST",
                    data: { userid: userId, amount: coinAmount, reason: coinReason },
                    url: "addCoins.php",
                    success: function(result) {
                        
                        if (result === 'Success') {
                            
                            alert(coinAmount + ' additional coins have been added for this user.');
                            
                            $(statusMessage).html("User's coins have been updated.").fadeIn(function() {
                                setTimeout(function() {
                                    $(statusMessage).fadeOut().html("");
                                }, CONSTANTS.MESSAGE_STATUS_TIMEOUT);
                            });
                            
                            //hide add coins div
                            $('.addCoinsWrapper').fadeOut();
                            
                        } else {
                            alert("Unable to update user's coin amount.");
                        }
                    },
                    complete: function(data) { }
                });    
            }
        });
    
    
        //*** REMOVE AVATAR
        $('body').on('click', '.avatarremove', function() {
            var useravatartoremove = $(this).attr('id');
            $.post("testput.php?removeavatar=" + useravatartoremove, function(data) {
                //alert("Avatar Removed: " + data);
                
                $(statusMessage).html('Removed Avatar for user id: ' + useravatartoremove).fadeIn(function() {
                    setTimeout(function() {
                        $(statusMessage).fadeOut().html("");
                    }, CONSTANTS.MESSAGE_STATUS_TIMEOUT);
                });
                
                alert('Removed Avatar for user with id: ' + useravatartoremove);
            });
        });
    
    
        //*** KILL BROADCAST
        $('body').on('click', '.userbroadcasting', function() {
            var broadcastertokill = $(this).attr('id');
            $.post("testput.php?userbroadcasting=" + broadcastertokill, function(data) {
                //alert("Broadcast Killed: " + data);
                
                $(statusMessage).html('Killed broadcast with id: ' + broadcastertokill).fadeIn(function() {
                    setTimeout(function() {
                        $(statusMessage).fadeOut().html("");
                    }, CONSTANTS.MESSAGE_STATUS_TIMEOUT);
                });
            });
        });
        
    
        //*** EMAIL USER
        $('body').on('click', '.emailUser', function() {
            var emailAddress = $(this).attr('id');
            var username = $(this).parents('.modal-body').find('#disciplineUsername').val();
            
             $.ajax({
              url: "templates/genericEmailLayout.php",
              type: "GET", 
              data: { userEmail: emailAddress },
              success: function(result) {
                    
                    $('.suspend-user-wrapper').empty().append(result).fadeIn();
                    
                    var buttons =  "<div class='form-group'>";
                    buttons += "<button type='button' id='genericEmailSendButton' class='btn btn-info emailSendButton'>Send Email</button>";
                    buttons += "&nbsp;&nbsp;&nbsp;";
                    buttons += "<button type='button' id='genericEmailCancelButton' class='btn btn-default emailCancelButton'>Cancel</button>";
                    buttons += "</div>";
                    
                    $('.emailUser').prop('disabled', true);
                    $('.suspend-user-wrapper').append(buttons);
                    
                },
                complete: function() { //*** email form is in place ***
            
                    var subjectField = $('input#subjectInput');
                    var messageField = $('textarea#genericEmailMessageBody');
                    
                    //update subject line while typing
                    $(subjectField).keyup(function() {
                        console.log('subject: ' + $(this).val());
                        var value = $(this).val();
                        $(subjectField).val(value);
                    });
                    
                    //update textera for message 
                    $(messageField).keyup(function() {
                        console.log('message: ' + $(this).val());
                       var value = $(this).val();
                       $(messageField).html(value);
                    });
                    
                    // prep for emailing
                    $('.emailSendButton').click(function() {
    //                    for testing 
    //                    console.log($(subjectField).text());
    //                    console.log($(subjectField).html());
    //                    console.log($(subjectField).val());
    //                    
    //                    console.log($(messageField).text());
    //                    console.log($(messageField).html());
    //                    console.log($(messageField).val());
                        
                        var subjectValue = $(subjectField).val();
                        var messageValue = $(messageField).html();
                        
                        //ensure subject has a value
                        if(subjectField.length === 0) {
                            subjectValue = "A Message from the Hang W/ Team";
                        }
                        
                        //ensure body is not empty
                        if(messageValue === 0) {
                            alert("Message body cannot be empty.");
                        }
                        else {
                            console.log('subject: ' + subjectValue);
                            console.log('message:' + messageValue);
                        }
                        
                        // fire off email 
                        $.ajax({
                            url: "genericemailuser.php",
                            type: "GET",
                            data: {emailaddress: emailAddress, subject: subjectValue, message: messageValue, username: username},
                            success: function(result) {
    
                               console.log('To: ' + emailAddress);
                               console.log('Subject:'  + subjectValue);
                               console.log('Message:' + messageValue);
                            },
                            complete: function() {
                        
                                $('.suspend-user-wrapper').fadeOut().empty();
                                $('.emailUser').prop('disabled', false);
                                
                                alert('An eamil message has bene sent to ' + emailAddress + '.');
                            },
                            error: function(error) {
                                alert('Email submission error: ' + JSON.stringify(error));
                            }
                        });
                    });  //end $.emailButton
                    
                    
                    //cancle button click event
                    $('.emailCancelButton').click(function() {
                       
                        //remove appended divs and reset email button
                        removeEmailDiv();
                    });
                    
                }}); //end $.ajax
         }); //end click on .emailUser

    
        //side effect - remove email wrapper div
        //if user clicks email button then then clicks close, the previous email form div is still injected into dom
        //and will appear for each popup when user clicks email button again.  
        $('.close').click(function() {
           // removeEmailDiv();
           removeEmailDiv();
    
        });
     
    
        
        // click on sidebar refresh button 
        $('#sidebar-refresh-button').on('click', function() {
            sidebarRefresh();
        });
   
        
    });
}(jQuery));